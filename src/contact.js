import React, {Component} from 'react';

class Contact extends Component {
    render() {
        return(
            <div>
                <h2> Talk with us</h2>
                <p>Send us a email </p>
                <ol>
                    <li class="contact-email">calebyang.officialwork@gmail.com</li>
                    <li class="contact-telephoneno">+ 65 61234 24388</li>
                </ol>
            </div>
        );
    }
}

export default Contact;