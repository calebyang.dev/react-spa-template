import React,{Component} from 'react';

class Services extends Component {
    render() {
        return(
            <div>
                <h2>Services</h2>
                <p>Tech development</p><br />
                <ol>
                    <li class="tech-webdev">Websites</li><br />
                    <li class="tech-mobileapp">Mobile Application</li><br />
                </ol><br />

                <p>Copywriting</p>
                <ol>
                    <li class="writing-tech">Technology</li><br />
                    <li class="writing-business">Business</li><br />
                    <li class="writing-blog">Blog</li><br />
                    <li class="writing-books">Books</li><br />
                    <li class="writing-articles">Articles</li><br />
                </ol><br />

                <p>Marketing</p>
                <ol>
                    <li class="marketing-campaigns">Marketing</li><br />
                    <li class="marketing-poster">Poster</li><br />
                    <li class="marketing-videos">Video</li><br />
                </ol><br />

            </div>
        );
    }
}

export default Services;