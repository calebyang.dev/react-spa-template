import React, {Component} from 'react';
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";

import Home from './Home';
import Portfolio from './portfolio';
import Contact from './contact';
import Services from './services';
import Articles from './article';

class Main extends Component {
    render() {
        return (
            <HashRouter>
                <div>
                    <h2>React SPA</h2>
                        <ul className="header">
                            <li><NavLink exact to="/">Home</NavLink></li>
                            <li><NavLink to="/contact">Contact Us</NavLink></li>
                            <li><NavLink to="/portfolio">Portfolio</NavLink></li>
                            <li><NavLink to ="/services">Services</NavLink></li>
                            <li><NavLink to="/articles">Articles</NavLink></li>
                        </ul>
                    <div className="content">
                        <Route path="/" component={Home}/>
                        <Route path="/contact" component={Contact}/>
                        <Route path="/portfolio" component={Portfolio}/>
                        <Route path ="/services" services={Services}/>
                        <NavLink to="/articles" articles={Articles}/>
                    </div>
                </div>
        </HashRouter>
        );
    }
}

export default Main;