import React,{Component} from 'react';

class Articles extends Component {
    render() {
        return(
            <div>
                <h2>Article</h2>
                <p>Web development</p><br />
                <ol>
                    <li class="web-dev-react">React</li><br />
                    <li class="web-dev-angular">Angular</li><br />
                    <li class="web-dev-vue">VueJs</li><br />
                    <li class="web-dev-pwa">PWA</li><br />
                </ol><br />

                <p>Mobile Applications</p>
                <ol>
                    <li class="mobile-android">Android</li><br />
                    <li class="mobile-IOS">IOS</li><br />
                    <li class="mobile-reactnative">React Native</li><br />
                    <li class="mobile-xamarin">Xamarin</li><br />
                </ol><br />

                <p>Artificial Intelligence</p>
                <ol>
                    <li class="ai-nlp">Natural Language Processing</li><br />
                    <li class="ai-dl">Deep Learning</li><br />
                </ol><br />

            </div>
        );
    }
}

export default Articles;